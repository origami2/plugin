var fnHelpers = require('origami-js-function-helpers');
var debug = require('debug')('origami:Plugin');

function plainError(obj) {
  if (obj instanceof Error) {
    return obj.message;
  }
  
  return obj;
}

function Plugin(api) {
  if (typeof(api) !== 'function') throw new Error('API is required to be a function');

  this.api = api;
  this.apiName = api.name;
  this.methods = {};

  for (var methodName in api.prototype) {
    this.methods[methodName] = fnHelpers.getFunctionArgumentNames(api.prototype[methodName]);
  }
}

Plugin.prototype.createInstance = function (context, token) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var args = fnHelpers
      .getFunctionArgumentNames(self.api)
      .map(function (argName) {
        return context[argName];
      });

    var target;

    try {
      target = Object.create(self.api.prototype);
      
      self.api.apply(target, args);
    } catch (e) {
      debug('%s constructor: error %s', self.apiName, e);
      console.error(e);
      
      return reject(plainError(e));
    }

    resolve(target);
  });
};

Plugin.prototype.invokeMethod = function (stackToken, context, methodName, params) {
  var self = this;

  return new Promise(function (resolve, reject) {
    self
    .createInstance(context, stackToken)
    .then(function (instance) {
      var method = self.api.prototype[methodName];

      var args = fnHelpers
        .getFunctionArgumentNames(method)
        .map(function (argName) {
          return params[argName];
        });

      try {
        var result = method.apply(instance, args);

        if (result instanceof Promise) {
          result
          .then(resolve)
          .catch(function (err) {
            debug('%s.%s: error %s', self.apiName, methodName, err);
            
            reject(plainError(err));
          });
        } else {
          resolve(result);
        }
      } catch (e) {
        debug('%s.%s: error %s', self.apiName, methodName, e);
        
        reject(plainError(e));
      }
    })
    .catch(function (e) {
      debug('%s.%s: error %s', self.apiName, methodName, e);
      
      reject(plainError(e));
    });
  });
};

Plugin.prototype.describeMethods = function () {
  return this.methods;
};

Plugin.prototype.getName = function () {
  return this.apiName;
};

module.exports = Plugin;
