# Origami plugin

### Purpose

Provide generic methods around a plugin implementation for describing it and invoking its dynamically.

### Usage

```javascript
function MyPlugin() {
}

MyPlugin.prototype.echo = function (what) {
  return Promise.resolve(what);
}

var Plugin = require('origami-plugin');

var plugin1 = new Plugin(MyPlugin);
```