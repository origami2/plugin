var Plugin = require('..');
var assert = require('assert');
var crypto = require('origami-crypto-utils');
var testData = require('./testData');

function StubPlugin (
  fold,
  constructorError,
  constructorStringError,
  methodError,
  methodStringError,
  local1
) {
  this.fold = fold;
  this.methodError = methodError;
  this.methodStringError = methodStringError;
  this.local1 = local1;

  if (constructorStringError) throw constructorStringError;
  if (constructorError) throw new Error('my constructor error');
}

StubPlugin.prototype.echo = function (what) {
  if (this.methodError) throw new Error('my method error');
  if (this.methodStringError) throw this.methodStringError;

  if (!this.fold) return Promise.reject('fold is required');

  return Promise.resolve(what);
};

StubPlugin.prototype.echo2 = function (what) {
  return this.echo(what);
};

describe('Plugin', function () {
  var stackToken = crypto.encryptAndSign(
    {
      attr1: '1'
    },
    testData.stack.privateKey,
    testData.stack.publicKey
  );

  describe('constructor', function () {
    it('requires an API', function () {
      assert
        .throws(
          function () {
            new Plugin();
          },
          /API is required to be a function/
        );
    });

    it('instantiates', function () {
      var target = new Plugin(
        StubPlugin
      );

      assert(target);
    });
  });

  describe('.createInstance', function () {
    it('returns a promise', function () {
      var target = new Plugin(
        StubPlugin
      );

      var promise = target.createInstance(
        {
        }
      );

      assert(promise);
      assert(promise instanceof Promise);
    });

    it('creates an instance', function (done) {
      var target = new Plugin(
        StubPlugin
      );

      target
      .createInstance(
        {
          fold: 'fold1'
        }
      )
      .then(function (instance) {
        try {
          assert('fold1', instance.fold1);

          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });

  describe('.invokeMethod', function () {
    it('returns a promise', function () {
      var target = new Plugin(
        StubPlugin
      );

      var promise = target
      .invokeMethod(
        stackToken,
        {
          fold: 'fold1'
        },
        'echo',
        {
          what: 'this'
        }
      );

      assert(promise);
      assert(promise instanceof Promise);
    });

    it('invokes method', function (done) {
      var target = new Plugin(
        StubPlugin
      );

      target
      .invokeMethod(
        stackToken,
        {
          fold: 'fold1'
        },
        'echo',
        {
          what: 'this'
        }
      )
      .then(
        function (result) {
          try {
            assert.deepEqual(
              'this',
              result
            );

            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });

    it('invokes method in the context of itself', function (done) {
      var target = new Plugin(
        StubPlugin
      );

      target
      .invokeMethod(
        stackToken,
        {
          fold: 'fold1'
        },
        'echo2',
        {
          what: 'this'
        }
      )
      .then(
        function (result) {
          try {
            assert.deepEqual(
              'this',
              result
            );

            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  });
  
  describe('.describeMethods', function () {
    it('returns methods with parameter names', function () {
      var target = new Plugin(
        StubPlugin
      );

      assert.deepEqual(
        {
          'echo': [ 'what' ],
          'echo2': [ 'what' ]
        },
        target.describeMethods()
      );
    });
  });
  
  describe('.getName', function () {
    it('returns plugin function name', function () {
      var target = new Plugin(
        StubPlugin
      );

      assert.equal(
        'StubPlugin',
        target.getName()
      );
    });
  });
});
